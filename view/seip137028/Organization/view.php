<?php
include_once('../../../vendor/autoload.php');
use App\bitm\seip137028\summary\summary;

$views=new summary();
$views->prepare($_GET);
$itemveiw=$views->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>emails</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" >
    <br>
    <center><a href="index.php" class="btn btn-success" role="button">Home</a>
    <a href="create.php" class="btn btn-warning" role="button">create New summary</a>
    <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
    </center>
    <h2><center><?php echo $itemveiw->name?></center></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $itemveiw->id?></li>
        <li class="list-group-item">name: <?php echo $itemveiw->name?></li>
        <li class="list-group-item">summary: <?php echo $itemveiw->summary?></li>

    </ul>
</div>

</body>
</html>
