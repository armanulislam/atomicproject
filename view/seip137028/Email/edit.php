<?php
include_once ('../../../vendor/autoload.php');
use App\bitm\seip137028\Email\Email;
use App\bitm\seip137028\Message\Message;
use App\bitm\seip137028\Utility\Utility;


$update=new Email();
$update->prepare($_GET);
$updated=$update->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>emails</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit email</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit email:</label>
            <input type="hidden" name="id"  value="<?php echo $updated->id?>">
            <input type="text" name="email" class="form-control"  placeholder="Enter new email" value="<?php echo
            $updated->email?>">
        </div>

        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>

</body>
</html>
