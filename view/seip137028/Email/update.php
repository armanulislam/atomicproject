<?php
include_once('../../../vendor/autoload.php');

use App\bitm\seip137028\Email\Email;
use App\bitm\seip137028\Message\Message;
use App\bitm\seip137028\Utility\Utility;

$update=new Email();
$update->prepare($_POST);
$update->edit();