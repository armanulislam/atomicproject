<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Create your profile</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <h3>Enter your name:</h3>
            <input type="text" name="title" class="form-control" id="name" placeholder="Enter your name">
            <h3>Upload file:</h3>
            <input type="file" name="image" class="form-control" id="image" placeholder="Upload file">

        </div>

        <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div>

</body>
</html>
