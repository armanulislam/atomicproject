<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>create user hobby</h2>
    <form role="form" action="store.php" method="post">
        <div class="input-group">
            <label><input type="checkbox" name="mark[]" value="Football"> Playing Football</label>
        </div>
        <div class="input-group">
            <label><input type="checkbox" name="mark[]" value="Hiking"> Hiking</label>
        </div>
        <div class="input-group">
            <label><input type="checkbox" name="mark[]" value="Travelling"> Travelling</label>
        </div>
        <div class="input-group">
            <label><input type="checkbox" name="mark[]" value="Fishing"> Fishing</label>
        </div>
        <div class="input-group">
            <input type="submit" class="btn btn-primary" value="Submit">
        </div>
    </form>
</div>

</body>
</html>

