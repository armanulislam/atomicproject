<?php
include_once ('../../../vendor/autoload.php');
use App\bitm\seip137028\Hobby\Hobby;
$allhobby=new Hobby();
$all_hobby=$allhobby->index();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Hobby list</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>all hobby</h2>
    <a href="create.php" class="btn btn-warning" role="button">Create again</a>  <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
<!--    <form role="form">-->
<!--        <div class="form-group">-->
<!--            <br><label for="sel1">Select homw many items you want to show (select one):</label>-->
<!--            <!-- <select class="form-control"  name="itemPerPage" >-->
<!--                 <option>5</option>-->
<!--                 <option selected>10</option>-->
<!--                 <option >15</option>-->
<!--                 <option>20</option>-->
<!--                 <option>25</option>-->
<!--             </select>-->
<!--            <input type="number" class="form-control" name="itemPerPage"><br>-->
<!---->
<!--            <button type="submit">Go!</button>-->
<!---->
<!--        </div>-->
<!--    </form>-->
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>hobby</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($all_hobby as $hobby){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $hobby['id']?></td>
                <td><?php echo $hobby['hobby']?></td>
                <td><a href="view.php?id=<?php echo $hobby['id'] ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $hobby['id'] ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $hobby['id'] ?>"  class="btn btn-info" role="button">Trash</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
<!--    <ul class="pagination">-->
<!--        <li><a href="#">Prev</a></li>-->
<!--        --><?php //echo $pagination?>
<!--        <li><a href="#">Next</a></li>-->
<!--    </ul>-->
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>

