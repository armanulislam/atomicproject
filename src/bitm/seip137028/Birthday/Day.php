<?php

namespace App\bitm\seip137028\Birth;
include_once('../../../vendor/autoload.php');
use App\bitm\seip137028\Message\Message;
use App\bitm\seip137028\Utility\Utility;
class Birth
{
    public $id;
    public $date;
    public $name;
    public $deleted_At;
    public $conn;
    public $allbirthData = array();
    public $allTrashedbirthData = array();

    function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    public function index()
    {
        $sql = "SELECT * FROM `birth` where deleted_at is null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allbirthData variable and return it to show the data
        // in index.php file
// we must declare allbirthData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allbirthData[] = $row;

        return $this->allbirthData;
    }


    public function prepare($data="")
    {
        if (array_key_exists('date', $data))
            $this->date = $data['date'];


        if (array_key_exists('name', $data))
            $this->name = $data['name'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];
    }

    public function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`birthday` (`name`, `date`)
 VALUES ('".$this->name."', '".$this->date."')";

        $result = mysqli_query($this->conn, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->birth;

        //return " I am storing data ";
    }


    public function update()
    {
        $query = "UPDATE `birth` SET `name` = '" . $this->name . "', `date` = '" . $this->date . "'
        WHERE `birthday`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

   public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`birth` WHERE `birth`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::Message("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::Message("error");
    }

    public function view()
    {
        $query = "SELECT * FROM `birth` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

   public function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `birth` SET `deleted_at` = '".$this->deletedAt."' WHERE `birth`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

   public function trashed()
    {
        $sql = "SELECT * FROM `birth` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allbirthData variable and return it to show the data
        // in index.php file
// we must declare allbirthData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allTrashedbirthData[] = $row;

        return $this->allTrashedbirthData;
    }

    function recover ()
    {
        $sql = "update birth set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update birth set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `birth` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `birth` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allbirth = array();
        $query="SELECT * FROM `birth` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allbirth[] = $row;
        }

        return $_allbirth;

    }
}