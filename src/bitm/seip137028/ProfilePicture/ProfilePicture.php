<?php
namespace App\bitm\seip137028\ProfilePicture;

class ProfilePicture{
    public $conn;
    public $id="";
    public $name="";
    public $image_name="";
    
    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }
    public function Prepare($data=""){
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("image",$data)){
            $this->image_name=$data["image"];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        return $this;
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`profilepicture` (`name`, `images`) VALUES ('".$this->name."', '".$this->image_name."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }


}